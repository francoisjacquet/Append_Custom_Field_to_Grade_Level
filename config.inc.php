<?php
/**
 * Plugin configuration interface
 *
 * @package Append Custom Field to Grade Level plugin
 */

// Check the script is called by the right program & plugin is activated.
if ( $_REQUEST['modname'] !== 'School_Setup/Configuration.php'
	|| ! $RosarioPlugins['Append_Custom_Field_to_Grade_Level']
	|| $_REQUEST['modfunc'] !== 'config' )
{
	$error[] = _( 'You\'re not allowed to use this program!' );

	echo ErrorMessage( $error, 'fatal' );
}

// Note: no need to call ProgramTitle() here!

if ( ! empty( $_REQUEST['save'] ) )
{
	if ( ! empty( $_REQUEST['values']['config'] )
		&& $_POST['values']
		&& AllowEdit() )
	{
		foreach ( (array) $_REQUEST['values']['config'] as $column => $value )
		{
			if ( $column === 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN_2'
				&& ! DBGetOne( "SELECT 1
					FROM config
					WHERE TITLE='APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN_2'
					AND SCHOOL_ID='0'" ) )
			{
				// Update version 2.0: force insert config for School ID 0.
				DBInsert(
					'config',
					[ 'SCHOOL_ID' => '0', 'TITLE' => 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN_2' ]
				);
			}

			// Update config value.
			Config( $column, $value );
		}

		$note[] = button( 'check' ) . '&nbsp;' . _( 'The plugin configuration has been modified.' );
	}

	// Unset save & values & redirect URL.
	RedirectURL( [ 'save', 'values' ] );
}

if ( empty( $_REQUEST['save'] ) )
{
	echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] .
			'&tab=plugins&modfunc=config&plugin=Append_Custom_Field_to_Grade_Level&save=true' ) . '" method="POST">';

	echo ErrorMessage( $note, 'note' );

	echo ErrorMessage( $error, 'error' );

	echo '<br />';

	$school_title = '';

	PopTable(
		'header',
		dgettext( 'Append_Custom_Field_to_Grade_Level', 'Append Custom Field to Grade Level' )
	);

	echo '<table class="width-100p">';

	// Text, Pull-Down, and Auto Pull-Down field types (no need to format those types before display)
	$fields_RET = DBGet( "SELECT cf.ID,cf.TITLE
		FROM custom_fields cf,student_field_categories sfc
		WHERE cf.TYPE IN('text','select','autos')
		AND cf.CATEGORY_ID=sfc.ID
		ORDER BY sfc.SORT_ORDER IS NULL,sfc.SORT_ORDER,sfc.TITLE,
			cf.SORT_ORDER IS NULL,cf.SORT_ORDER,cf.TITLE" );

	$field_options = [ 'USERNAME' => _( 'Username' ) ];

	if ( ! empty( $RosarioModules['Hostel'] ) )
	{
		$field_options['HOSTEL_ROOM'] = dgettext( 'Hostel', 'Hostel' ) . ' - ' . dgettext( 'Hostel', 'Room' );
	}

	foreach ( (array) $fields_RET as $field )
	{
		$field_options[ 'CUSTOM_' . $field['ID'] ] = ParseMLField( $field['TITLE'] );
	}

	echo '<tr><td>' . SelectInput(
		Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN' ),
		'values[config][APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN]',
		_( 'Student Field' ),
		$field_options,
		'N/A',
		'required'
	) . '</td></tr>';

	echo '<tr><td>' . SelectInput(
		Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN_2' ),
		'values[config][APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN_2]',
		_( 'Student Field' ) . ' (2)',
		$field_options,
		'N/A',
		''
	) . '</td></tr>';

	echo '<tr><td>' . TextInput(
		Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_SEPARATOR' ),
		'values[config][APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_SEPARATOR]',
		dgettext( 'Append_Custom_Field_to_Grade_Level', 'Separator' ),
		'size="3" maxlength="3"'
	) . '</td></tr>';

	echo '<tr><td>' . CheckboxInput(
		Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_LIST_ONLY' ),
		'values[config][APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_LIST_ONLY]',
		dgettext( 'Append_Custom_Field_to_Grade_Level', 'Student Listing only' ),
		'',
		false,
		button( 'check' ),
		button( 'x' )
	) . '</td></tr>';

	$sort_options = [ '' => _( 'Grade Level' ) ];

	if ( isset( $field_options[ Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN' ) ] ) )
	{
		$sort_options['COLUMN'] = _( 'Student Field' )
			. ' &mdash; ' . $field_options[ Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN' ) ];
	}

	if ( isset( $field_options[ Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN_2' ) ] ) )
	{
		$sort_options['COLUMN_2'] = _( 'Student Field' ) . ' (2)'
			. ' &mdash; ' . $field_options[ Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_COLUMN_2' ) ];
	}

	echo '<tr><td>' . SelectInput(
		Config( 'APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_SORT_ORDER' ),
		'values[config][APPEND_CUSTOM_FIELD_TO_GRADE_LEVEL_SORT_ORDER]',
		dgettext( 'Append_Custom_Field_to_Grade_Level', 'Sort Column by' ),
		$sort_options
	) . '</td></tr>';

	echo '</table>';

	PopTable( 'footer' );

	echo '<br /><div class="center">' . SubmitButton() . '</div></form>';
}
